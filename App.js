/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  FlatList,
  View,
  Text,
  StatusBar,
} from 'react-native';
import COLORS from './utils/colors'
import { createAppContainer , createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import PropertyPage from './components/PropertyPage';

import SearchResults from './components/SearchResults'; 
import SearchBarComp from './components/SearchBar'
import SearchPage from './components/SearchPage';
import styles from './Styles';
import NewsSection from './components/NewsSection';
import HomeScreen from './components/HomeScreen';
import LifestyleSection from './components/LifestyleSection';
import { Card, ListItem, Button, Icon, Divider } from 'react-native-elements'
export default class App extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };
  render() {
    console.ignoredYellowBox = ['Warning: `-[RCTRootView cancelTouches]`'];
    return <AppContainer />;
  }
}

const RootStack = createStackNavigator(
  {
    Home: { 
      screen: HomeScreen,
      navigationOptions: {
        title: 'Home',
        headerStyle: {
          backgroundColor: COLORS.primaryBlue,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          color: 'white',
        },
      }
    },
    SearchSearchPage: { 
      screen: SearchPage,
      navigationOptions: {
        title: 'Search Page',
        headerStyle: {
          backgroundColor: COLORS.primaryBlue,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          color: 'white',
        },
      }
    },
    SearchBarComp,
    SearchResults: { 
      screen: SearchResults,
      navigationOptions: {
        title: 'Results',
        headerTintColor: 'white',
        headerStyle: {
          backgroundColor: COLORS.primaryBlue,
          tex: 'white'
        },
        headerTitleStyle: {
          color: 'white',
        },
      }
    },
    PropertyPage: {
      screen: PropertyPage,
      navigationOptions: {
        title: 'Property',
        headerTintColor: 'white',
        headerStyle: {
          backgroundColor: COLORS.primaryBlue,
          tex: 'white'
        },
        headerTitleStyle: {
          color: 'white',
        },
      }
    }
  },

);
const AppContainer = createAppContainer(RootStack);

