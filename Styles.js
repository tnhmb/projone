
import React from 'react';
import {Colors, StyleSheet} from 'react-native';
import COLORS from './utils/colors'
export default StyleSheet.create({
  scrollView: {
    
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    //backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitleNews: {
    fontSize: 24,
    fontWeight: '600',
    margin: 5
    //color: Colors.black,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    //color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    //color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  homeTitleContainer: {

  },
  homeIcon: {
    marginTop: 20,
  },
  homeTitle: {
    marginTop: 5,
    fontSize: 24,
    textAlign: 'center',
    fontWeight: '600',
    color: 'white'
  },
  footer: {
    //color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  description: {
    marginBottom: 20,
    fontSize: 18,
    textAlign: 'center',
    color: COLORS.dimGray
  },
  viewBorder: {
    borderWidth: 1,
    borderColor: COLORS.lightGray,
  },
  steelBlueContainer: {
    backgroundColor: COLORS.primaryBlue,
  },

  container: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 10,
    paddingTop: 20,
    paddingRight: 20,
    paddingLeft: 20,
    paddingBottom:10,
    marginTop: 40,
    marginRight: 10,
    marginLeft: 10,
    marginBottom: 20,
    alignItems: 'stretch'
  },
  flowRight: {
    flexDirection: 'row',
    alignItems: 'stretch',
    flex: 1,
    justifyContent: 'center',   
  },
  flowRightFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'space-around',
  },
  flowBottom: {
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  slider: {
    marginTop: 15,
    overflow: 'visible' // for custom animations
  },
  searchInput: {
    height: 36,
    padding: 4,
    marginRight: 5,
    flexGrow: 1,
    alignItems: 'stretch',
    fontSize: 18,
    borderWidth: 1,
    borderColor: COLORS.summerSky,
    borderRadius: 8,
    color: COLORS.summerSky,
  },
  image: {
    width: 300,
    height: 200,
  },
  newsSectionItemText: {
    
    padding:10,
    flex: 1, 
    flexWrap: 'wrap'
  },
  imageHeader: {
    width: '100%',
    height: 200,
  },
  newsSectionHeaderItem: {
    backgroundColor: COLORS.revell, 
    flexDirection: 'column',
    height: 300,
    flex: 1, 
    alignItems: "stretch", 
    justifyContent: "center",
      alignItems: 'flex-start',
      paddingBottom: 10,
      margin: 5,

  },
  newsSectionItem: {
    backgroundColor: COLORS.revell, 
    flexDirection: 'column',
    width: 300,
    height: 300,
    flex: 1, 
    alignItems: "stretch", 
    justifyContent: "center",
      alignItems: 'flex-start',
      paddingBottom: 10,
      margin: 5,

  },
  divider: { 
    backgroundColor: 'gray', 
    marginTop: 10
  },
  thumb: {
    width: 80,
    height: 80,
    marginRight: 10
  },
  textContainer: {
    flex: 1,
    alignSelf: 'flex-end',
    backgroundColor: COLORS.translucent,
  },
  separator: {
    height: 1,
    backgroundColor: COLORS.gainsboro
  },
  price: {
    fontSize: 25,
    fontWeight: 'bold',
    color: COLORS.summerSky
  },
  title: {
    fontSize: 18,
    padding: 10,
    fontWeight: 'bold',
  },
   propertyDescription: {
    fontSize: 14,
    paddingLeft: 10,
    paddingBottom:10,
    color: COLORS.dimGray
  },
  iconText: {
    fontSize: 14,
    marginLeft:5,
    marginRight:5,
    color: COLORS.dimGray
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10,
  },
  columnContainer: {
    padding: 10,
    flexDirection: 'column',
  },
  cardImage: {
    height: 200,
  },
  cardContainer: { 
    flex: 1, 
    alignItems: "stretch", 
    justifyContent: "center" 
  }, 
  textOverlay: { 
    position:'absolute', 
    top: 0, 
    left: 10, 
    right: 0, 
    bottom: 10, 
    justifyContent: 'flex-end', 
    alignItems: 'flex-start'
  },
  textImage: { 
    color: COLORS.summerSky, 
    paddingLeft: 10,
    paddingBottom:10,
    fontWeight: 'bold',
    fontSize: 18
  },
  icon: {
    margin:20
  },
  iconStar: {
    flex:1,
    alignItems: 'flex-end'
  },
  listItem: { 
    backgroundColor: COLORS.revell, 
    borderRadius: 10, 
    margin: 10, 
    overflow: "hidden"
    },
    loading: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center'
  }, 
  propertyContainer: {
    borderRadius: 10,
    backgroundColor: COLORS.revell,
    margin: 10, 
  },
  morgageIcon: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  morgageArrow: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  propertyInfoList: {
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  map: {
    borderRadius: 10,
    width: '100%',
    margin: 10,
    height: 250,
  },
});
