import React from 'react'
import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  FlatList,
  ScrollView,
  Text,
} from 'react-native';
import styles from '../Styles';
import { Card, Icon } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import { Dimensions } from 'react-native'; 
import COLORS from '../utils/colors'
export default class GalleryCarousel extends React.Component {
    _renderItem ({item, index}) {
    return (
        <View style={styles.slide}>
            <Image style={styles.cardImage} source={{ uri: item.thumbnailUrl }} />
            </View>
        );
    }
    render() {
    const item = this.props.item
        return (
            <Carousel 
                ref={(c) => { this._carousel = c; }}
                data={item.medias}
                renderItem={this._renderItem}
                sliderWidth={Dimensions.get('window').width-20}
                itemWidth={Dimensions.get('window').width-20}
            />
        );
    }
}