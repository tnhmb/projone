
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  FlatList,
  View,
  Text,
  StatusBar,
} from 'react-native';
//import {StackNavigator} from 'react-navigation'
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import SearchPage from './SearchPage';
import styles from '../Styles';
import NewsSection from './NewsSection';
import LifestyleSection from './LifestyleSection';
import { Card, ListItem, Button, Icon, Divider } from 'react-native-elements'
class HomeScreen extends React.Component {
  
  render() {
    return (
       <ScrollView contentContainerStyle={styles.scrollView}>
        
        <View style = {styles.steelBlueContainer}>
        <View style={styles.homeIcon}>
          <Icon type='font-awesome' color= 'white' name='home' size = {30} />
          </View>
            <Text style = {styles.homeTitle}>
              Property Finder
            </Text>
          


          <SearchPage navigation={this.props.navigation}/>
        </View>
          <NewsSection/>
          <Divider style={styles.divider} />
          <LifestyleSection/>
        </ScrollView>
    );
  }
}

export default HomeScreen