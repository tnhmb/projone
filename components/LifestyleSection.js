import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  FlatList,
  SectionList,
  View,
  Text,
  Image,
  StatusBar,
} from 'react-native';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import lifestyle from '../models/Lifestyle';
import styles from '../Styles';

class LifestyleSection extends React.Component {
   state = {
      lifestyle: lifestyle
   };
   render() {
      return (
         <View>
         <Text style = {styles.sectionTitleNews}>Lifestyle</Text>
            <View key = {this.state.lifestyle[2].id} style = {styles.newsSectionHeaderItem}>
               <Image source={{ uri: this.state.lifestyle[2].image }} style={styles.imageHeader} />
               <Text style = {styles.newsSectionItemText}>{this.state.lifestyle[2].description}</Text>
            </View>
            <ScrollView horizontal={true}>
               {
                  this.state.lifestyle.map((item, index) => (
                     <View key = {item.id} style = {styles.newsSectionItem}>
                        <Image source={{ uri: item.image }} style={styles.image} />
                        <Text style = {styles.newsSectionItemText}>{item.description}</Text>
                     </View>
                  ))
               }
            </ScrollView>
         </View>
      );
   };
}
export default LifestyleSection;