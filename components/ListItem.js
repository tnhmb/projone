import React from 'react'
import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  FlatList,
  ScrollView,
  Text,
} from 'react-native';
import styles from '../Styles';
import { Card, Icon } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import { Dimensions } from 'react-native'; 
import { createAppContainer , createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import COLORS from '../utils/colors';
import GalleryCarousel from './GalleryCarousel';
import { getSingleProperty } from '../services/FetchProperties'

export default class ListItem extends React.PureComponent {
constructor() {
   super();
   this.state = { 
       toggle: true,
       iconName: "star-o"
    };
 }
  _onPress = () => {
      getSingleProperty()
          .then((res) => {
            this.props.navigation.push('PropertyPage', {
              title: 'Property',
              item: res,
            });
        }) 
        .catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
            throw error;
        });
    
  }
  _onPressIcon = () => {
      this.setState( state => ({
      toggle: !state.toggle
    }));
    if (this.state.toggle) {
        this.setState({
            iconName:'star'
        })
    } else {
        this.setState({
            iconName:'star-o'
        })
    }
  }

  render() {
    const item = this.props.item
    console.log('item tre prop: ' + item.title)
    const price = item.prices[0].currency+ ' ' +item.prices[0].max;
    
    return (
      
        <View style={styles.cardContainer}>
            <View style={styles.listItem}>
                <View>
                    <GalleryCarousel item={item} />

                </View>
                <TouchableHighlight
                    onPress={this._onPress}
                    underlayColor={COLORS.gainsboro}>
                    <View >
                        <Text style={styles.title}>{item.title}</Text>
                        <Text style={styles.textImage}>{price}</Text>
                        <Text style={styles.propertyDescription}>{item.propertyType}</Text>
                        <Text style={styles.propertyDescription}>Built-up size: {item.attributes.builtUp} sq. ft.</Text>
                        <View style= {styles.rowContainer}>
                            <Icon type='font-awesome' name='bed' color='gray' size = {14} />
                            <Text style={styles.iconText}>{item.attributes.bedroom}</Text>
                            <Icon type='font-awesome' name='bath' color='gray' size = {14} />
                            <Text style={styles.iconText}>{item.attributes.bathroom}</Text>
                            <Icon type='font-awesome' name='car' color='gray' size = {14} />
                            <Text style={styles.iconText}>{item.attributes.carPark}</Text>
                            <View style={styles.iconStar}>
                                <Icon type='font-awesome' name={this.state.iconName} color='gray' size = {14} onPress = {this._onPressIcon} />
                            </View>
                        </View>
                        
                    </View>
                </TouchableHighlight>
            </View>
            
        </View>

      
    );
  }
}