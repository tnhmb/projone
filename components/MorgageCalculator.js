import React from 'react';
import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  FlatList,
  ScrollView,
  Text,
} from 'react-native';
import styles from '../Styles';
import { Card, Icon } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import { Dimensions } from 'react-native'; 
import COLORS from '../utils/colors';
import ListItem from './ListItem';

export default class MorgageCalculator extends React.Component {
    render() {
        return (
            <View style = {styles.propertyContainer}>
                    <View style = {styles.rowContainer}>
                        <View style={styles.morgageIcon}>
                            <Icon type='font-awesome' name='calculator' color = {COLORS.dimGray}/>
                        </View>
                        <View style = {styles.columnContainer}>
                            <Text style={styles.title} > Morgage Calculator </Text>
                            <Text style={styles.propertyDescription} > Monthley Repayment: </Text>
                        </View>
                        <View style={styles.morgageArrow}>
                            <Icon type='font-awesome' name='arrow-right' color = {COLORS.dimGray}/>
                        </View>
                    </View>
                </View>
        );
    }
} 