import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  FlatList,
  SectionList,
  View,
  Text,
  Image,
  StatusBar,
} from 'react-native';
import { Card, ListItem, Button, Icon, Divider } from 'react-native-elements'

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import news from '../models/News';
import styles from '../Styles';

class NewsSection extends React.Component {
   state = {
      news: news
   };
   render() {
      return (
         <View>
         <Text style = {styles.sectionTitleNews}>News</Text>
            <View key = {this.state.news[2].id} style = {styles.newsSectionHeaderItem}>
               <Image source={{ uri: this.state.news[2].image }} style={styles.imageHeader} />
               <Text style = {styles.newsSectionItemText}>{this.state.news[2].description}</Text>
            </View>
            <ScrollView horizontal={true}>
               {
                  this.state.news.map((item, index) => (
                     <View key = {item.id} style = {styles.newsSectionItem}>
                        <Image source={{ uri: item.image }} style={styles.image} />
                        <Text style = {styles.newsSectionItemText}>{item.description}</Text>
                     </View>
                  ))
               }
            </ScrollView>
         </View>
      );
   };
}
export default NewsSection;