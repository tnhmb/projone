import React from 'react'
import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  FlatList,
  ScrollView,
  Text,
} from 'react-native';

import styles from '../Styles';
import { Card, Icon } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import { Dimensions } from 'react-native'; 
import COLORS from '../utils/colors';
import ListItem from './ListItem';
import Moment from 'moment';
export default class PropertyInfo extends React.Component {
    render() {
        const item = this.props.item
        Moment.locale('en');
        return(
            <View style = {styles.propertyContainer}>
                    <View style = {styles.rowContainer}>
                        <View style={styles.morgageIcon}>
                        </View>
                        <View style = {styles.columnContainer}>
                            <Text style={styles.title} > Property Information </Text>
                                <View style={styles.rowContainer}>
                                    <Text style={styles.propertyDescription} > Land title Type: </Text>
                                    <View style={styles.propertyInfoList}>
                                        <Text style={styles.propertyDescription} > {item.attributes.landTitleType} </Text>
                                    </View>
                                </View>
                                <View style={styles.rowContainer}>
                                    <Text style={styles.propertyDescription} > Tenure: </Text>
                                    <View style={styles.propertyInfoList}>
                                        <Text style={styles.propertyDescription} > {item.attributes.tenure} </Text>
                                    </View>
                                </View>
                                <View style={styles.rowContainer}>
                                    <Text style={styles.propertyDescription} > Land Area: </Text>
                                    <View style={styles.propertyInfoList}>
                                        <Text style={styles.propertyDescription} > {item.attributes.builtUp} </Text>
                                    </View>
                                </View>
                                <View style={styles.rowContainer}>
                                    <Text style={styles.propertyDescription} > QuikPro No.: </Text>
                                    
                                    <View style={styles.propertyInfoList}>
                                        <Text style={styles.propertyDescription} > {item.attributes.buildingId} </Text>
                                    </View>
                                </View>
                                <View style={styles.rowContainer}>
                                    <Text style={styles.propertyDescription} > Posted Date: </Text>
                                    <View style={styles.propertyInfoList}>
                                        <Text style={styles.propertyDescription} > {Moment(item.updatedAt).format('d MMM YYYY')} </Text>
                                    </View>
                                </View>
                        </View>
    
                    </View>
                </View>
        );
    }
}