import React from 'react'
import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  FlatList,
  ScrollView,
  Text,
} from 'react-native';
import styles from '../Styles';
import { Card, Icon } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import { Dimensions } from 'react-native'; 
import COLORS from '../utils/colors';
import ListItem from './ListItem';
import MorgageCalculator from './MorgageCalculator';
import PropertyInfo from './PropertyInfo';
import MapView from 'react-native-maps';
export default class PropertyPage extends React.Component {


    render() {
       const { navigation } = this.props;
    
        const item = navigation.getParam('item', 'NO-ID')
        console.log('...data ' + item.title)
        return (
            <ScrollView>
                <ListItem item = {item}/>
                <MorgageCalculator/>
                <PropertyInfo item = {item}/>
                
                <MapView
                    style={styles.map}
                    scrollEnabled={true}
                    zoomEnabled={true}
                    pitchEnabled={true}
                    initialRegion={{
                        latitude: item.address.lat,
                        longitude: item.address.lng,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                >
                    <MapView.Marker
                        title={item.title}
                        description={item.propertyCatType}
                        coordinate={{
                            latitude: item.address.lat,
                            longitude: item.address.lng,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                    />
                </MapView>
                
            </ScrollView>
            
        );
    }
}