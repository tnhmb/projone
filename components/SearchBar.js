import React from 'react';
import { getProperties } from '../services/FetchProperties';
import Icon from "react-native-vector-icons/MaterialIcons";
import styles from '../Styles';
import HomeScreen from './HomeScreen';
import SearchResults from './SearchResults';
import App from '../App'
import { createAppContainer , createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import COLORS from '../utils/colors'
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  ActivityIndicator,
  Image,
} from 'react-native';
export default class SearchBarComp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      message: '',
    };

  }
  
  updateSearch = search => {
    this.setState({ search });
  };

 _onSearchPressed = () => {
     this.setState({
         isLoading:true
     })
    const query = urlForQueryAndPage('place_name', this.state.searchString, 1);
    this._executeQuery(query);
  };
    _executeQuery = (query) => {
    this.setState({ isLoading: true });
    getProperties(query)
          .then((res) => {
              this.setState({
                isLoading:false
            })
             console.log(this.props)
              this.props.navigation.push('SearchResults', {
              title: 'Results',
              item: res,
            });
        }) 
        .catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
            throw error;
        });
  };
  _onSearchTextChanged = (event) => {
    this.setState({ searchString: event.nativeEvent.text });
  };



  _handleResponse = (response) => {
    this.setState({ isLoading: false , message: '' });
    if (response.application_response_code.substr(0, 1) === '1') {
      this.props.navigation.push('SearchResults', {
              title: 'Results',
              item: res,
            });
    } else {
      this.setState({ message: 'Location not recognized; please try again.'});
    }
  };

 

  render() {
    const { search } = this.state;
    const spinner = this.state.isLoading ?
      <ActivityIndicator style= {styles.loadoing }size='large'/> : null;
    return (
      <View style={styles.flowRight}>
      <View style={styles.loading}>
            {spinner}
          </View>
          <TextInput
            style={styles.searchInput}
            value={this.state.searchString}
            onChange={this._onSearchTextChanged}
            placeholder='Search area'/>
          <Button 
            onPress= {this._onSearchPressed}
            color= {COLORS.summerSky}
            title='Go'
          />
          
        </View>
    );
  }
}

 function urlForQueryAndPage(key, value, pageNumber) {
  const data = {
      country: 'my',
      pretty: '1',
      encoding: 'json',
      listing_type: 'buy',
      action: 'search_listings',
      page: pageNumber,
  };
  data[key] = value;

  const querystring = Object.keys(data)
    .map(key => key + '=' + encodeURIComponent(data[key]))
    .join('&');

  return 'https://testdb-b267a.firebaseio.com/items.json' + querystring;
}
 