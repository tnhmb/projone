

'use strict';

import React, { Component } from 'react'
import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  FlatList,
  ScrollView,
  Text,
} from 'react-native';
import styles from '../Styles';
import ListItem from './ListItem';
import { SearchBar } from 'react-native-elements';

export default class SearchResults extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      search: ''
    };
  }

  updateSearch = search => {
    this.setState({ search });
  };
  _keyExtractor = (item, index) => (
    index.toString()
  )

  _renderItem = ({item, index}) => (
    
    <ListItem
      navigation={this.props.navigation}
      item={item}
      index={index}
      onPressItem={this._onPressItem}
    />
  );

  _onPressItem = (index) => {
    console.log("Pressed row: "+index);
  };

  render() {
    const { navigation } = this.props;
    
    const item = Object.values(navigation.getParam('item', 'NO-ID'))
    console.log('items: ' + item[1].title)
    
    return (
      <View>
      <SearchBar
        lightTheme round
        placeholder="Search area"
        onChangeText={this.updateSearch}
        value={this.state.search}
      />
      <FlatList
        data={item}
        keyExtractor= {this._keyExtractor}
        renderItem={this._renderItem}
      />
      </View>
      
    );
  }
}
