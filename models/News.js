import React from 'react'

const newsModel = [
         {'description': 'Lorem ipsum dolor sit amet, per in discere offendit accusamus. Pri ex ridens probatus necessitatibus, sumo civibus pri ut.', 'image':'https://picsum.photos/id/1/300/200', 'id': 1},
         {'description': 'Ne vel laudem eloquentiam, cu nec soleat laoreet suavitate. Vis dicam alterum an, ne affert aliquid noluisse qui, cu dicat facer aeque usu.', 'image':'https://picsum.photos/id/2/300/200','id': 2},
         {'description': 'Te nonumy iriure mel, eos purto consetetur at. His ne cibo quodsi offendit, ea solum omittantur eum. Impetus periculis ea eos, ne voluptua maiestatis mei. Quot essent eligendi an vim. Cu mei saperet probatus accusamus.', 'image':'https://picsum.photos/300/200','id': 3},
         {'description': 'Mea delenit accommodare et. Debitis fierent vituperata mel at, ex dico albucius quo, eam ea appareat menandri perpetua. Ut mundi sanctus explicari vim.', 'image':'https://picsum.photos/id/4/300/200','id': 4},
         {'description': 'No pericula intellegam duo, semper dolorum ut mea.', 'image':'https://picsum.photos/id/5/300/200','id': 5},
         {'description': 'Cum ea tota lucilius petentium. Eum te dolorum facilisi pertinax, equidem scriptorem quo ea.', 'image':'https://picsum.photos/id/6/300/200','id': 6},
         {'description': 'Mel an utroque meliore periculis, natum cetero constituto sit cu. Vim natum similique concludaturque ea, mundi adipisci reformidans ne sed. Sit ne velit laoreet placerat, ad sea possim discere.', 'image':'https://picsum.photos/id/7/300/200','id': 7},
         {'description': 'Mea ei qualisque incorrupte, laudem nonumes eu qui. Mei at aeque postea expetendis, integre detracto indoctum ex per, inermis percipit pericula no has.', 'image':'https://picsum.photos/id/8/300/200','id': 8},
         {'description': 'Usu ex diam epicuri, ex eius scaevola splendide vel. Natum eleifend in sit. Quot vidisse fabulas ne ius, animal pertinax mnesarchum at nam, quas indoctum ius ut. No wisi eleifend patrioque est, no cum sanctus partiendo complectitur. ', 'image':'https://picsum.photos/id/9/300/200','id': 9}
      ];
   

export default newsModel;