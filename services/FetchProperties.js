import React from 'react'

export const getProperties = (query) => {
    let sq = query.toLowerCase().trim();
    const URL = `https://testdb-b267a.firebaseio.com/items.json`;
    return fetch(URL)
            .then((res) => res.json());
};

export const getSingleProperty = () => {
    const URL = `https://luminous-inferno-7393.firebaseio.com/property.json`;
    return fetch(URL)
            .then((res) => res.json());
};

