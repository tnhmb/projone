const COLORS = {
    primaryBlue: '#4682b4',
    dimGray: '#656565',
    lightGray: '#d3d3d3',
    summerSky: '#48BBEC',
    tobago: '#2a4944',
    translucent: 'rgba(0, 0, 0, 0.5)',
    gainsboro: '#dddddd',
    revell: '#eee'
}
export default COLORS;